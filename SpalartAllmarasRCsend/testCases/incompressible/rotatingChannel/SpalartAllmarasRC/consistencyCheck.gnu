set term 'png'
set xl 'y/H'  font "Helvetica,20"
set yl '{/Symbol w_1_2}, {/Symbol w_2_1}'  font "Helvetica,20"
set tics font "Helvetica,15"
set key font "Helvetica,15"
set termoption dashlength 1.5
set grid

set output 'checkS_Omega.png'

plot 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:($12 - 0.5) w p  tit 'S_1_2 - {/Symbol W_3}', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:21 w p  tit '{/Symbol w_1_2}', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(-$14 + 0.5) w p  tit '-S_2_1 + {/Symbol W_3}', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:23 w p  tit '{/Symbol w_2_1}' 


set output 'checkSOmega_rtar.png'
set yl 'r*'  font "Helvetica,20"



plot 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(sqrt(2.0*($12*$12 + $14*$14))/(sqrt(2.0*(($12 - 0.5)**2 + (-$14 + 0.5)**2)))) w p lc rgb "red"  tit 'sqrt(2S_i_jS_i_j)/sqrt(2{/Symbol w}_i_j{/Symbol w}_i_j )', 'postProcessing/graphs/40000/line1_fr1_nuTilda_nut_rhat_rstar.xy' u 1:6 w p  tit 'r*'


set output 'checkHS.png'
set yl '-S_1_2{/Symbol W_3},S_2_1{/Symbol W_3}'  font "Helvetica,20"


plot 0.25 tit '{/Symbol W_3^2}' , -0.25 tit '-{/Symbol W_3^2}' ,'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:38  tit 'H_S_,_1_1', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(-$12*0.5) tit '-S_1_2{/Symbol W_3}', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:42 tit 'H_S_,_2_2' , 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:($14*0.5) tit 'S_2_1{/Symbol W_3}'


set output 'checkSH.png'


plot  0.25 tit '{/Symbol W_3^2}' , -0.25 tit '-{/Symbol W_3^2}' , 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:47  tit 'S_H_,_1_1', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(-$12*0.5) tit '-S_1_2{/Symbol W_3}', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:51 tit 'S_H_,_2_2' , 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:($14*0.5) tit 'S_2_1{/Symbol W_3}'  


set output 'checkWSD.png'
set yl 'WSD_1_1,WSD_2_2'  font "Helvetica,20"
set key top left

plot 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:29  tit 'WSD_1_1', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(2*$12*$21/ ($12**2 + $14**2 +  $21**2 + $23**2)**2) tit '{/Symbol w_1_2}S_1_2/D^4', 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:33 tit 'WSD_2_2' , 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(2*$14*$23/($12**2 + $14**2 +  $21**2 + $23**2)**2) tit '{/Symbol w_2_1}S_2_1/D^4'


set output 'checkWSDE_rhat.png'
set yl 'rhat'  font "Helvetica,20"


plot 'postProcessing/graphs/40000/line1_H_S_W_WSD_eimnSjnOm_ejmnSinOm.xy' u 1:(2*($29*$38 + $33*$42)) w p lc rgb "red"  tit 'rhat(Sij,omegaij)', 'postProcessing/graphs/40000/line1_fr1_nuTilda_nut_rhat_rstar.xy' u 1:5 w p  tit 'rhat'

set output 'checkrhat_fr1.png'
set yl 'f_r_1'  font "Helvetica,20"
set key top right

plot 'postProcessing/graphs/40000/line1_fr1_nuTilda_nut_rhat_rstar.xy' u 1:2 w p  tit 'f_r_1', 'postProcessing/graphs/40000/line1_fr1_nuTilda_nut_rhat_rstar.xy' u 1:((1+1)*2*$6/(1+$6)* ( 1- atan(12*$5) ) - 1.0) w p  tit 'fr1(rhat,r*)'


