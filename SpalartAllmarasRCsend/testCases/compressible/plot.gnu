pInfty = 100000
rhoInfty = 1.17
Uinfty = sqrt(290**2  + 13.73**2)


b_x02=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.20.csv | sort -nk3 | awk 'END {print $3}'")
b_x04=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.40.csv | sort -nk3 | awk 'END {print $3}'")
b_x06=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.60.csv | sort -nk3 | awk 'END {print $3}'")
b_x08=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.80.csv | sort -nk3 | awk 'END {print $3}'")
b_x095=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.950.csv | sort -nk3 | awk 'END {print $3}'")
b_y0=system("awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_yc00.csv | sort -nk2 | awk 'END {print $2}'")

set xl 'y/b' font "Helvetica,20"
set yl '-cp' font "Helvetica,20"
set tics font "Helvetica,18"
set key font "Helvetica,18"
set term png
set key bottom center
set yr [0:1.8]

set output 'Cpx0_2_paraview.png'

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.20.csv | sort -nk3" u (($4>0?$3:1/0)/b_x02):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_xc0.20.csv | sort -nk3" u (($4>0?$3:1/0)/b_x02):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' , 'Ma=0.87_alpa=26.7_x=0.2.dat' u 1:(-$2) w p  pt 6 lw 2 lc rgb "red" tit 'exp'


set output 'Cpx0_4_paraview.png'

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.40.csv | sort -nk3" u (($4>0?$3:1/0)/b_x04):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_xc0.40.csv | sort -nk3" u (($4>0?$3:1/0)/b_x04):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' , 'Ma=0.87_alpa=26.7_x=0.4.dat' u 1:(-$2) w p  pt 6 lw 2 lc rgb "red" tit 'exp'


set output 'Cpx0_6_paraview.png'

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.60.csv | sort -nk3" u (($4>0?$3:1/0)/b_x06):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_xc0.60.csv | sort -nk3" u (($4>0?$3:1/0)/b_x06):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' , 'Ma=0.87_alpa=26.7_x=0.6.dat' u 1:(-$2) w p  pt 6 lw 2 lc rgb "red" tit 'exp'


set output 'Cpx0_8_paraview.png'

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.80.csv | sort -nk3" u (($4>0?$3:1/0)/b_x08):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_xc0.80.csv | sort -nk3" u (($4>0?$3:1/0)/b_x08):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' , 'Ma=0.87_alpa=26.7_x=0.8.dat' u 1:(-$2) w p  pt 6 lw 2 lc rgb "red" tit 'exp'


set output 'Cpx0_95_paraview.png'

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_xc0.950.csv | sort -nk3" u (($4>0?$3:1/0)/b_x095):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_xc0.950.csv | sort -nk3" u (($4>0?$3:1/0)/b_x095):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' , 'Ma=0.87_alpa=26.7_x=0.95.dat' u 1:(-$2) w p  pt 6 lw 2 lc rgb "red" tit 'exp'


set output 'Cpy0_paraview.png'
set xl 'x/c' font "Helvetica,20"
set yr [-0.5:1.0]

plot "< awk '!/-/' Ma0.85alpha26.7PimpleCoarse/p_yc00.csv | sort -nk2" u (($4>0?$2:1/0)/b_y0):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SA' , "< awk '!/-/' Ma0.85alpha26.7PimpleCoarseRC/p_yc00.csv | sort -nk2" u (($4>0?$2:1/0)/b_y0):(-2*($1-pInfty)/rhoInfty/Uinfty**2) w l lw 3 tit 'SARC' 


