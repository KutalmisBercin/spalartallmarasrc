# trace generated using paraview version 5.6.3
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'OpenFOAMReader'
mfoam = OpenFOAMReader(FileName='./m.foam')
mfoam.MeshRegions = ['internalMesh']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [847, 458]

# show data in view
mfoamDisplay = Show(mfoam, renderView1)

# trace defaults for the display properties.
mfoamDisplay.Representation = 'Surface'
mfoamDisplay.ColorArrayName = [None, '']
mfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
mfoamDisplay.SelectOrientationVectors = 'None'
mfoamDisplay.ScaleFactor = 1.7658000946044923
mfoamDisplay.SelectScaleArray = 'None'
mfoamDisplay.GlyphType = 'Arrow'
mfoamDisplay.GlyphTableIndexArray = 'None'
mfoamDisplay.GaussianRadius = 0.08829000473022461
mfoamDisplay.SetScaleArray = [None, '']
mfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
mfoamDisplay.OpacityArray = [None, '']
mfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
mfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
mfoamDisplay.SelectionCellLabelFontFile = ''
mfoamDisplay.SelectionPointLabelFontFile = ''
mfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
mfoamDisplay.ScalarOpacityUnitDistance = 0.2514143537222057

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
mfoamDisplay.DataAxesGrid.XTitleFontFile = ''
mfoamDisplay.DataAxesGrid.YTitleFontFile = ''
mfoamDisplay.DataAxesGrid.ZTitleFontFile = ''
mfoamDisplay.DataAxesGrid.XLabelFontFile = ''
mfoamDisplay.DataAxesGrid.YLabelFontFile = ''
mfoamDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
mfoamDisplay.PolarAxes.PolarAxisTitleFontFile = ''
mfoamDisplay.PolarAxes.PolarAxisLabelFontFile = ''
mfoamDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
mfoamDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on mfoam
mfoam.CaseType = 'Decomposed Case'

# Properties modified on mfoamDisplay
mfoamDisplay.SetScaleArray = [None, 'Ma']

# Properties modified on mfoamDisplay
mfoamDisplay.OpacityArray = [None, 'Ma']

# Properties modified on mfoamDisplay
mfoamDisplay.OSPRayScaleArray = 'Ma'

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on mfoam
mfoam.MeshRegions = ['internalMesh', 'wing']
mfoam.CellArrays = ['Ma', 'T', 'U', 'alphat', 'nuTilda', 'nut', 'p', 'rDeltaT', 'rho', 'wallShearStress', 'yPlus']

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on mfoam
mfoam.MeshRegions = ['wing']

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on mfoam
mfoam.CellArrays = []

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on mfoam
mfoam.CellArrays = ['p']

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on mfoamDisplay
mfoamDisplay.SetScaleArray = [None, 'p']

# Properties modified on mfoamDisplay
mfoamDisplay.OpacityArray = [None, 'p']

# Properties modified on mfoamDisplay
mfoamDisplay.OSPRayScaleArray = 'p'

# set scalar coloring
ColorBy(mfoamDisplay, ('POINTS', 'p'))

# rescale color and/or opacity maps used to include current data range
mfoamDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
mfoamDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.RGBPoints = [14902.7890625, 0.231373, 0.298039, 0.752941, 82166.72265625, 0.865003, 0.865003, 0.865003, 149430.65625, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [14902.7890625, 0.0, 0.5, 0.0, 149430.65625, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# reset view to fit data
renderView1.ResetCamera()

# create a new 'Slice'
slice1 = Slice(Input=mfoam)
slice1.SliceType = 'Plane'
slice1.Triangulatetheslice = 0
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1)

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = ['POINTS', 'p']
slice1Display.LookupTable = pLUT
slice1Display.OSPRayScaleArray = 'p'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'None'
slice1Display.ScaleFactor = 0.011507850140333176
slice1Display.SelectScaleArray = 'p'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'p'
slice1Display.GaussianRadius = 0.0005753925070166588
slice1Display.SetScaleArray = ['POINTS', 'p']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'p']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.SelectionCellLabelFontFile = ''
slice1Display.SelectionPointLabelFontFile = ''
slice1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice1Display.DataAxesGrid.XTitleFontFile = ''
slice1Display.DataAxesGrid.YTitleFontFile = ''
slice1Display.DataAxesGrid.ZTitleFontFile = ''
slice1Display.DataAxesGrid.XLabelFontFile = ''
slice1Display.DataAxesGrid.YLabelFontFile = ''
slice1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice1Display.PolarAxes.PolarAxisTitleFontFile = ''
slice1Display.PolarAxes.PolarAxisLabelFontFile = ''
slice1Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice1.SliceType)

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.0, 0.11322694271802902, 0.0]

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.0, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice1Display
slice1Display.SelectScaleArray = 'None'

# Properties modified on slice1Display
slice1Display.GlyphTableIndexArray = 'None'

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.0981, 0.11322694271802902, 0.0]

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [0.0981, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(mfoam)

# create a new 'Slice'
slice2 = Slice(Input=mfoam)
slice2.SliceType = 'Plane'
slice2.Triangulatetheslice = 0
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice2Display = Show(slice2, renderView1)

# trace defaults for the display properties.
slice2Display.Representation = 'Surface'
slice2Display.ColorArrayName = ['POINTS', 'p']
slice2Display.LookupTable = pLUT
slice2Display.OSPRayScaleArray = 'p'
slice2Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice2Display.SelectOrientationVectors = 'None'
slice2Display.ScaleFactor = 0.011507850140333176
slice2Display.SelectScaleArray = 'p'
slice2Display.GlyphType = 'Arrow'
slice2Display.GlyphTableIndexArray = 'p'
slice2Display.GaussianRadius = 0.0005753925070166588
slice2Display.SetScaleArray = ['POINTS', 'p']
slice2Display.ScaleTransferFunction = 'PiecewiseFunction'
slice2Display.OpacityArray = ['POINTS', 'p']
slice2Display.OpacityTransferFunction = 'PiecewiseFunction'
slice2Display.DataAxesGrid = 'GridAxesRepresentation'
slice2Display.SelectionCellLabelFontFile = ''
slice2Display.SelectionPointLabelFontFile = ''
slice2Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice2Display.DataAxesGrid.XTitleFontFile = ''
slice2Display.DataAxesGrid.YTitleFontFile = ''
slice2Display.DataAxesGrid.ZTitleFontFile = ''
slice2Display.DataAxesGrid.XLabelFontFile = ''
slice2Display.DataAxesGrid.YLabelFontFile = ''
slice2Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice2Display.PolarAxes.PolarAxisTitleFontFile = ''
slice2Display.PolarAxes.PolarAxisLabelFontFile = ''
slice2Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice2Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice2.SliceType)

# Properties modified on slice2.SliceType
slice2.SliceType.Origin = [0.1962, 0.11322694271802902, 0.0]

# Properties modified on slice2.SliceType
slice2.SliceType.Origin = [0.1962, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(mfoam)

# create a new 'Slice'
slice3 = Slice(Input=mfoam)
slice3.SliceType = 'Plane'
slice3.Triangulatetheslice = 0
slice3.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice3.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice3Display = Show(slice3, renderView1)

# trace defaults for the display properties.
slice3Display.Representation = 'Surface'
slice3Display.ColorArrayName = ['POINTS', 'p']
slice3Display.LookupTable = pLUT
slice3Display.OSPRayScaleArray = 'p'
slice3Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice3Display.SelectOrientationVectors = 'None'
slice3Display.ScaleFactor = 0.011507850140333176
slice3Display.SelectScaleArray = 'p'
slice3Display.GlyphType = 'Arrow'
slice3Display.GlyphTableIndexArray = 'p'
slice3Display.GaussianRadius = 0.0005753925070166588
slice3Display.SetScaleArray = ['POINTS', 'p']
slice3Display.ScaleTransferFunction = 'PiecewiseFunction'
slice3Display.OpacityArray = ['POINTS', 'p']
slice3Display.OpacityTransferFunction = 'PiecewiseFunction'
slice3Display.DataAxesGrid = 'GridAxesRepresentation'
slice3Display.SelectionCellLabelFontFile = ''
slice3Display.SelectionPointLabelFontFile = ''
slice3Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice3Display.DataAxesGrid.XTitleFontFile = ''
slice3Display.DataAxesGrid.YTitleFontFile = ''
slice3Display.DataAxesGrid.ZTitleFontFile = ''
slice3Display.DataAxesGrid.XLabelFontFile = ''
slice3Display.DataAxesGrid.YLabelFontFile = ''
slice3Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice3Display.PolarAxes.PolarAxisTitleFontFile = ''
slice3Display.PolarAxes.PolarAxisLabelFontFile = ''
slice3Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice3Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice3.SliceType)

# Properties modified on slice3.SliceType
slice3.SliceType.Origin = [0.2943, 0.11322694271802902, 0.0]

# Properties modified on slice3.SliceType
slice3.SliceType.Origin = [0.2943, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(mfoam)

# create a new 'Slice'
slice4 = Slice(Input=mfoam)
slice4.SliceType = 'Plane'
slice4.Triangulatetheslice = 0
slice4.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice4.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice4Display = Show(slice4, renderView1)

# trace defaults for the display properties.
slice4Display.Representation = 'Surface'
slice4Display.ColorArrayName = ['POINTS', 'p']
slice4Display.LookupTable = pLUT
slice4Display.OSPRayScaleArray = 'p'
slice4Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice4Display.SelectOrientationVectors = 'None'
slice4Display.ScaleFactor = 0.011507850140333176
slice4Display.SelectScaleArray = 'p'
slice4Display.GlyphType = 'Arrow'
slice4Display.GlyphTableIndexArray = 'p'
slice4Display.GaussianRadius = 0.0005753925070166588
slice4Display.SetScaleArray = ['POINTS', 'p']
slice4Display.ScaleTransferFunction = 'PiecewiseFunction'
slice4Display.OpacityArray = ['POINTS', 'p']
slice4Display.OpacityTransferFunction = 'PiecewiseFunction'
slice4Display.DataAxesGrid = 'GridAxesRepresentation'
slice4Display.SelectionCellLabelFontFile = ''
slice4Display.SelectionPointLabelFontFile = ''
slice4Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice4Display.DataAxesGrid.XTitleFontFile = ''
slice4Display.DataAxesGrid.YTitleFontFile = ''
slice4Display.DataAxesGrid.ZTitleFontFile = ''
slice4Display.DataAxesGrid.XLabelFontFile = ''
slice4Display.DataAxesGrid.YLabelFontFile = ''
slice4Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice4Display.PolarAxes.PolarAxisTitleFontFile = ''
slice4Display.PolarAxes.PolarAxisLabelFontFile = ''
slice4Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice4Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice4Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice4.SliceType)

# set active source
SetActiveSource(mfoam)

# Properties modified on slice4.SliceType
slice4.SliceType.Origin = [0.3924, 0.11322694271802902, 0.0]

# Properties modified on slice4.SliceType
slice4.SliceType.Origin = [0.3924, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Slice'
slice5 = Slice(Input=mfoam)
slice5.SliceType = 'Plane'
slice5.Triangulatetheslice = 0
slice5.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice5.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice5Display = Show(slice5, renderView1)

# trace defaults for the display properties.
slice5Display.Representation = 'Surface'
slice5Display.ColorArrayName = ['POINTS', 'p']
slice5Display.LookupTable = pLUT
slice5Display.OSPRayScaleArray = 'p'
slice5Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice5Display.SelectOrientationVectors = 'None'
slice5Display.ScaleFactor = 0.011507850140333176
slice5Display.SelectScaleArray = 'p'
slice5Display.GlyphType = 'Arrow'
slice5Display.GlyphTableIndexArray = 'p'
slice5Display.GaussianRadius = 0.0005753925070166588
slice5Display.SetScaleArray = ['POINTS', 'p']
slice5Display.ScaleTransferFunction = 'PiecewiseFunction'
slice5Display.OpacityArray = ['POINTS', 'p']
slice5Display.OpacityTransferFunction = 'PiecewiseFunction'
slice5Display.DataAxesGrid = 'GridAxesRepresentation'
slice5Display.SelectionCellLabelFontFile = ''
slice5Display.SelectionPointLabelFontFile = ''
slice5Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice5Display.DataAxesGrid.XTitleFontFile = ''
slice5Display.DataAxesGrid.YTitleFontFile = ''
slice5Display.DataAxesGrid.ZTitleFontFile = ''
slice5Display.DataAxesGrid.XLabelFontFile = ''
slice5Display.DataAxesGrid.YLabelFontFile = ''
slice5Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice5Display.PolarAxes.PolarAxisTitleFontFile = ''
slice5Display.PolarAxes.PolarAxisLabelFontFile = ''
slice5Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice5Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice5Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice5.SliceType
slice5.SliceType.Origin = [0.466, 0.11322694271802902, 0.0]

# Properties modified on slice5.SliceType
slice5.SliceType.Origin = [0.466, 0.11322694271802902, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice5.SliceType)

# set active source
SetActiveSource(slice4)

# set active source
SetActiveSource(slice3)

# set active source
SetActiveSource(slice2)

# set active source
SetActiveSource(slice1)

# hide data in view
Hide(slice5, renderView1)

# hide data in view
Hide(slice4, renderView1)

# hide data in view
Hide(slice3, renderView1)

# hide data in view
Hide(slice2, renderView1)

# set active source
SetActiveSource(mfoam)

# create a new 'Slice'
slice6 = Slice(Input=mfoam)
slice6.SliceType = 'Plane'
slice6.Triangulatetheslice = 0
slice6.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice6.SliceType.Origin = [0.2479290529154241, 0.11322694271802902, 0.0]

# show data in view
slice6Display = Show(slice6, renderView1)

# trace defaults for the display properties.
slice6Display.Representation = 'Surface'
slice6Display.ColorArrayName = ['POINTS', 'p']
slice6Display.LookupTable = pLUT
slice6Display.OSPRayScaleArray = 'p'
slice6Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice6Display.SelectOrientationVectors = 'None'
slice6Display.ScaleFactor = 0.011507850140333176
slice6Display.SelectScaleArray = 'p'
slice6Display.GlyphType = 'Arrow'
slice6Display.GlyphTableIndexArray = 'p'
slice6Display.GaussianRadius = 0.0005753925070166588
slice6Display.SetScaleArray = ['POINTS', 'p']
slice6Display.ScaleTransferFunction = 'PiecewiseFunction'
slice6Display.OpacityArray = ['POINTS', 'p']
slice6Display.OpacityTransferFunction = 'PiecewiseFunction'
slice6Display.DataAxesGrid = 'GridAxesRepresentation'
slice6Display.SelectionCellLabelFontFile = ''
slice6Display.SelectionPointLabelFontFile = ''
slice6Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
slice6Display.DataAxesGrid.XTitleFontFile = ''
slice6Display.DataAxesGrid.YTitleFontFile = ''
slice6Display.DataAxesGrid.ZTitleFontFile = ''
slice6Display.DataAxesGrid.XLabelFontFile = ''
slice6Display.DataAxesGrid.YLabelFontFile = ''
slice6Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
slice6Display.PolarAxes.PolarAxisTitleFontFile = ''
slice6Display.PolarAxes.PolarAxisLabelFontFile = ''
slice6Display.PolarAxes.LastRadialAxisTextFontFile = ''
slice6Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(mfoam, renderView1)

# show color bar/color legend
slice6Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice6.SliceType
slice6.SliceType.Normal = [0.0, 1.0, 0.0]

# Properties modified on slice6.SliceType
slice6.SliceType.Normal = [0.0, 1.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=slice6.SliceType)

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.0, 0.0]

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.0, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice6Display
slice6Display.SelectScaleArray = 'None'

# Properties modified on slice6Display
slice6Display.GlyphTableIndexArray = 'None'

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.0, 0.001]

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.0, 0.001]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.001, 0.001]

# Properties modified on slice6.SliceType
slice6.SliceType.Origin = [0.2479290529154241, 0.001, 0.001]

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice6, renderView1)

# save data
SaveData('./p_xc0.2.csv', proxy=slice6)

# set active source
SetActiveSource(slice1)

# set active source
SetActiveSource(slice2)

# set active source
SetActiveSource(slice3)

# set active source
SetActiveSource(slice4)

# set active source
SetActiveSource(slice5)

# set active source
SetActiveSource(slice6)

# set active source
SetActiveSource(slice2)

# show data in view
slice2Display = Show(slice2, renderView1)

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice1, renderView1)

# save data
SaveData('./p_xc0.4.csv', proxy=slice2)

# set active source
SetActiveSource(slice3)

# show data in view
slice3Display = Show(slice3, renderView1)

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice2, renderView1)

# set active source
SetActiveSource(slice1)

# show data in view
slice1Display = Show(slice1, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice1, renderView1)

# show data in view
slice1Display = Show(slice1, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(slice3, renderView1)

# save data
SaveData('./p_xc0.2.csv', proxy=slice1)

# set active source
SetActiveSource(slice2)

# show data in view
slice2Display = Show(slice2, renderView1)

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice1, renderView1)

# save data
SaveData('./p_xc0.4.csv', proxy=slice2)

# set active source
SetActiveSource(slice3)

# show data in view
slice3Display = Show(slice3, renderView1)

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice3, renderView1)

# show data in view
slice3Display = Show(slice3, renderView1)

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(slice2, renderView1)

# save data
SaveData('./p_xc0.6.csv', proxy=slice3)

# set active source
SetActiveSource(slice4)

# show data in view
slice4Display = Show(slice4, renderView1)

# show color bar/color legend
slice4Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice3, renderView1)

# set active source
SetActiveSource(slice3)

# show data in view
slice3Display = Show(slice3, renderView1)

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice4, renderView1)

# save data
SaveData('./p_xc0.6.csv', proxy=slice3)

# set active source
SetActiveSource(slice4)

# show data in view
slice4Display = Show(slice4, renderView1)

# show color bar/color legend
slice4Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice3, renderView1)

# save data
SaveData('./p_xc0.8.csv', proxy=slice4)

# set active source
SetActiveSource(slice5)

# show data in view
slice5Display = Show(slice5, renderView1)

# show color bar/color legend
slice5Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice4, renderView1)

# save data
SaveData('./p_xc0.95.csv', proxy=slice5)

# set active source
SetActiveSource(slice6)

# show data in view
slice6Display = Show(slice6, renderView1)

# show color bar/color legend
slice6Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(slice5, renderView1)

# save data
SaveData('./p_yc0.csv', proxy=slice6)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [0.286656053866521, 0.08456534649987164, 1.0436339119869922]
renderView1.CameraFocalPoint = [0.2479290529154241, 0.11322694271802902, 0.0]
renderView1.CameraViewUp = [0.0010173181040822293, 0.9996236156728369, 0.02741517924507768]
renderView1.CameraParallelScale = 0.27040001412732984

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).