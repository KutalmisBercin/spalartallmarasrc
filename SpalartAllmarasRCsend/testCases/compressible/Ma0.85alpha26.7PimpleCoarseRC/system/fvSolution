/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v1912                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p
    {
        solver          GAMG;
        smoother        GaussSeidel;
        tolerance       1e-8;
        relTol          0.1;
    }

    pFinal
    {
        $p;
        relTol          0;
    }

    "(rho|U|k|omega|e|nuTilda)"
    {
        solver          PBiCGStab;
        preconditioner  DILU;
        tolerance       1e-8;
        relTol          0.01;
    }

    "(rho|U|k|omega|e|nuTilda)Final"
    {
        $U;
        relTol          0;
    }
}

SIMPLE
{
    residualControl
    {
        p               1e-4;
        U               1e-4;
        "(k|omega|e)"   1e-4;
    }

    nNonOrthogonalCorrectors 0;
    pMinFactor      0.05;
    pMaxFactor      3;
}

PIMPLE
{
    consistent      yes;
    transonic       yes;
    nCorrectors              2;
    nNonOrthogonalCorrectors 0;
    nOuterCorrectors         1;
    pMinFactor      0.1;
    pMaxFactor      2;
    maxCo           2.0;
}

relaxationFactors
{
    equations
    {
        ".*"     1;
    }
}

// ************************************************************************* //
