/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2006                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      fvOptions;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

limitT
{
    type       limitTemperature;
    min        101;
    max        1000;
    selectionMode all;
}

codedSource
{
    type            scalarCodedSource;
    selectionMode   all;

    fields          (nuTilda);
    name            rotationCorrecton;
    
    libs        ("libutilityFunctionObjects.so");
    
    codeCorrect
    #{
    #};
    
    codeConstrain
    #{
    #};
    
    codeOptions
    #{
       -I$(LIB_SRC)/finiteVolume/lnInclude \
       -I$(LIB_SRC)/TurbulenceModels/turbulenceModels/lnInclude \
       -I$(LIB_SRC)/TurbulenceModels/incompressible/lnInclude \
       -I$(LIB_SRC)/TurbulenceModels/compressible/lnInclude \
       -I$(LIB_SRC)/transportModels \
       -I$(LIB_SRC)/transportModels/compressible/lnInclude \
       -I$(LIB_SRC)/transportModels/incompressible/singlePhaseTransportModel 
    #};


    codeInclude
    #{
        #include "volFieldsFwd.H"
        #include "fvCFD.H" 
        #include "turbulentTransportModel.H"
        #include "SpalartAllmaras.H"
        #include "RASModel.H"
        #include "TurbulenceModel.H"
        #include "CompressibleTurbulenceModel.H"
        #include "compressibleTransportModel.H"
        #include "wallDist.H"

    #};
    
    codeData
    #{
       const volScalarField::Internal& y_;
    #};


    codeAddSup
    #{
    
        const volVectorField& U = mesh().lookupObject<volVectorField>("U");
        
        const turbulenceModel& turbulence =
        mesh().lookupObject<turbulenceModel>("turbulenceProperties");
        
        y_(wallDist::New(this->mesh_).y());
        
        volScalarField V
        (
            IOobject
            (
               mesh_.V().name(),
               mesh().time().timeName(),
               mesh_,
               IOobject::NO_READ,
               IOobject::NO_WRITE,
               false
            ),
            mesh_,
            dimensionedScalar(mesh_.V().dimensions(), Zero),
            calculatedFvPatchField<scalar>::typeName
         );
 
        V.ref() = mesh_.V();
        
        
        scalarField& nuTildaSource = eqn.source();
        volScalarField& nuTilda = eqn.psi();
        
        volScalarField chi(nuTilda/turbulence.nu());
        volScalarField chi3(pow3(chi));
        volScalarField fv1 (chi3/(chi3 + pow3(7.1)));
        volScalarField:: Internal fv2 (scalar(1) - chi/(scalar(1) + chi*fv1));
        volScalarField::Internal Omega
        (
            sqrt(scalar(2))*mag(skew(fvc::grad(U)().v()))
        );
        
        volScalarField kappay (0.41*y_);
        
        volScalarField::Internal Stilda
        (
            max
            (
               Omega + fv2*nuTilda/sqr(kappay),
               0.3*Omega
            )
        );
        
        scalar cr1(1.0);
        scalar cr2(12.0);
        scalar cr3(1.0);
        
        volTensorField gradU (Foam::fvc::grad(U));
        volTensorField S (0.5  * ( gradU + gradU.T()));
        volTensorField W (0.5  * ( gradU.T() - gradU ));
        volTensorField WS (W & S.T());
        volScalarField S2 (2.0 * (S && S));
        volScalarField W2 (2.0 * (W && W));
        volScalarField D2 (S2 + W2);
        volScalarField rstar (sqrt(S2) / sqrt(W2));
        surfaceScalarField phi(Foam::fvc::interpolate(U)&mesh().Sf());
        
        volTensorField DSDT (1.0 / V * (Foam::fvc::ddt(S) + Foam::fvc::div(phi,S)));
        volTensorField WSD (2.0 * WS / sqr(D2));
        volScalarField rhat ( WSD && DSDT);
        volScalarField fr1 ( (1.0 + cr1)*2.0*rstar / (1.0 + rstar)* (1.0 - cr3*Foam::atan(cr2*rhat)  ) - cr1 );
        
        fvMatrix<scalar> RCEqn
        (
            Stilda*0.1355*nuTilda - fvm::Sp(Stilda*0.1355*fr1, nuTilda) 
        );
        
        eqn =- RCEqn;


        
/*        
        // Start time
        const scalar startTime = 2.0;

        // Retrieve the x component of the cell centres
        const scalarField& cellx = mesh_.C().component(0);

        // Only apply when we have reached the start time
        if (time.value() > startTime)
        {
            // Apply the source
            forAll(cellx, i)
            {
                // cell volume specific source
                heSource[i] += 1e5*sin(200*cellx[i])*V[i];
            };
        }*/
    #};
}

//************************************************************************** //
