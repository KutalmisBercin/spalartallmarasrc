/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2011-2016 OpenFOAM Foundation
    Copyright (C) 2019-2021 OpenCFD Ltd.
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "SpalartAllmarasRC.H"
#include "fvOptions.H"
#include "bound.H"
#include "wallDist.H"
#include "IOMRFZoneList.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
namespace RASModels
{

// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //

template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::chi() const
{
    return nuTilda_/this->nu();
}


template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::fv1
(
    const volScalarField& chi
) const
{
    const volScalarField chi3(pow3(chi));

    return chi3/(chi3 + pow3(Cv1_));
}

template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::fr1( ) const
{
    
    const auto* MRFZones =
    this->mesh().template 
             cfindObject<IOMRFZoneList>("MRFProperties");
             
    volVectorField Urel (this->U_);
    volVectorField& UrelRef (Urel);
    MRFZones->MRFZoneList::makeRelative(UrelRef);
    surfaceScalarField phi(Foam::fvc::interpolate(UrelRef)&this->mesh_.Sf());
    
    volVectorField Omega (this->OmegaRefFrame());
    volTensorField gradU (Foam::fvc::grad(UrelRef));
    
    // transpose to be consistent with notation of Shur et al. 2000
    gradU = gradU.T();
    volTensorField S ("S", scalar(0.5)  * ( gradU + gradU.T()));
    volTensorField dSdt (Foam::fvc::ddt(S));
    volTensorField H ("H", *Omega);
    volTensorField W ("W", scalar(0.5)  * ( gradU - gradU.T()  + 2.0*H));
    volTensorField WS (W & S.T());
    volScalarField S2 (scalar(2.0) * (S && S));
    volScalarField W2 (scalar(2.0) * (W && W));
    volScalarField D2 (scalar(0.5) * (S2 + W2));
    dimensionedScalar smallW2 (W2.dimensions(), SMALL);
    volScalarField rstar ("rstar", sqrt(S2) / (sqrt(W2 + smallW2)));
    
    
    volTensorField ejmnSinOm ("ejmnSinOm", S & H.T());
    volTensorField eimnSjnOm ("eimnSjnOm", H & S.T());
    
    
    volTensorField DSDT (dSdt + Foam::fvc::div(phi,S));
    
    dimensionedScalar smallD2 (D2.dimensions()*D2.dimensions(), SMALL);
    
    volTensorField WSD ("WSD", scalar(2.0) * WS / (sqr(D2) + smallD2 ));
    volScalarField rhat ("rhat", WSD && (DSDT + eimnSjnOm + ejmnSinOm));
    
    if (this->mesh_.time().writeTime())
    {
        ejmnSinOm.write();
        eimnSjnOm.write();
        H.write();
        WSD.write();
        W.write();
        S.write();
        rhat.write();
        rstar.write();
    }
    
    return ( (scalar(1.0) + Cr1_)*scalar(2.0)*rstar / (scalar(1.0) + rstar)* (scalar(1.0) - Cr3_*Foam::atan(Cr2_*rhat)) - Cr1_ );
    
}

template<class BasicTurbulenceModel>
volVectorField SpalartAllmarasRC<BasicTurbulenceModel>::OmegaRefFrame() const
{
    
    //volVectorField Omega (this->mesh_, dimless/dimTime, vector::zero);
    
    volVectorField Omega
    (
        IOobject
        (
             "Omega",
              this->runTime_.timeName(),
              this->mesh_,
              IOobject::NO_READ,
              IOobject::NO_WRITE
        ),
        this->mesh_,
        dimensionedVector("Omega", dimless/dimTime, Zero)
    );
    
    const auto* MRFZones =
    this->mesh().template 
             cfindObject<IOMRFZoneList>("MRFProperties");
             
    if (!MRFZones && MRFZones->MRFZoneList::size() == 0 && debug)
    {
        Info << "Unable to find MRFProperties in the database. " << endl
             << "Omega set to zero" << endl;
        return Omega;
    }

    for (label i=0; i < MRFZones->MRFZoneList::size(); i++)
    {
        const dictionary& MRFZoneDict = 
            MRFZones->IOdictionary::subDict(MRFZones->MRFZoneList::operator[](i).name());

        const label cellZoneIndex = 
            this->mesh_.cellZones().findIndex(MRFZoneDict.get<word>("cellZone"));
        
          const labelList& cells = this->mesh_.cellZones()[cellZoneIndex];
          
          forAll(cells, cellI)
          {
              label celli = cells[cellI];
              Omega[celli] = MRFZones->MRFZoneList::operator[](i).Omega();
          }
     }
     
    if (this->mesh_.time().writeTime())
    {
        Info << "write OmegaRefFrame" << endl;
        Omega.write();
    }
         
    
    return Omega;
    
}

template<class BasicTurbulenceModel>
tmp<volScalarField::Internal> SpalartAllmarasRC<BasicTurbulenceModel>::fv2
(
    const volScalarField::Internal& chi,
    const volScalarField::Internal& fv1
) const
{
    return scalar(1) - chi/(scalar(1) + chi*fv1);
}


template<class BasicTurbulenceModel>
tmp<volScalarField::Internal> SpalartAllmarasRC<BasicTurbulenceModel>::Stilda()
const
{
    const volScalarField chi(this->chi());

    const volScalarField fv1(this->fv1(chi));

    const volScalarField::Internal Omega
    (
        ::sqrt(scalar(2))*mag(skew(fvc::grad(this->U_)().v()))
    );

    return
    (
        max
        (
            Omega + fv2(chi(), fv1())*nuTilda_()/sqr(kappa_*y_),
            Cs_*Omega
        )
    );
}


template<class BasicTurbulenceModel>
tmp<volScalarField::Internal> SpalartAllmarasRC<BasicTurbulenceModel>::fw
(
    const volScalarField::Internal& Stilda
) const
{
    const volScalarField::Internal r
    (
        min
        (
            nuTilda_
           /(
               max
               (
                   Stilda,
                   dimensionedScalar(Stilda.dimensions(), SMALL)
               )
              *sqr(kappa_*y_)
            ),
            scalar(10)
        )
    );

    const volScalarField::Internal g(r + Cw2_*(pow6(r) - r));

    return
        g*pow
        (
            (scalar(1) + pow6(Cw3_))/(pow6(g) + pow6(Cw3_)),
            scalar(1)/scalar(6)
        );
}


template<class BasicTurbulenceModel>
void SpalartAllmarasRC<BasicTurbulenceModel>::correctNut()
{
    this->nut_ = nuTilda_*this->fv1(this->chi());
    this->nut_.correctBoundaryConditions();
    fv::options::New(this->mesh_).correct(this->nut_);

    BasicTurbulenceModel::correctNut();
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class BasicTurbulenceModel>
SpalartAllmarasRC<BasicTurbulenceModel>::SpalartAllmarasRC
(
    const alphaField& alpha,
    const rhoField& rho,
    const volVectorField& U,
    const surfaceScalarField& alphaRhoPhi,
    const surfaceScalarField& phi,
    const transportModel& transport,
    const word& propertiesName,
    const word& type
)
:
    eddyViscosity<RASModel<BasicTurbulenceModel>>
    (
        type,
        alpha,
        rho,
        U,
        alphaRhoPhi,
        phi,
        transport,
        propertiesName
    ),

    sigmaNut_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "sigmaNut",
            this->coeffDict_,
            scalar(2)/scalar(3)
        )
    ),
    kappa_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "kappa",
            this->coeffDict_,
            0.41
        )
    ),

    Cb1_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cb1",
            this->coeffDict_,
            0.1355
        )
    ),
    Cb2_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cb2",
            this->coeffDict_,
            0.622
        )
    ),
    Cw1_(Cb1_/sqr(kappa_) + (scalar(1) + Cb2_)/sigmaNut_),
    Cw2_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cw2",
            this->coeffDict_,
            0.3
        )
    ),
    Cw3_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cw3",
            this->coeffDict_,
            2.0
        )
    ),
    Cv1_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cv1",
            this->coeffDict_,
            7.1
        )
    ),
    Cs_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cs",
            this->coeffDict_,
            0.3
        )
    ),
    Cr1_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cr1",
            this->coeffDict_,
            1.0
        )
    ),
    Cr2_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cr2",
            this->coeffDict_,
            12.0
        )
    ),
    Cr3_
    (
        dimensioned<scalar>::getOrAddToDict
        (
            "Cr3",
            this->coeffDict_,
            1.0
        )
    ),

    nuTilda_
    (
        IOobject
        (
            "nuTilda",
            this->runTime_.timeName(),
            this->mesh_,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        this->mesh_
    ),

    y_(wallDist::New(this->mesh_).y())
{
    if (type == typeName)
    {
        this->printCoeffs(type);
    }
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class BasicTurbulenceModel>
bool SpalartAllmarasRC<BasicTurbulenceModel>::read()
{
    if (eddyViscosity<RASModel<BasicTurbulenceModel>>::read())
    {
        sigmaNut_.readIfPresent(this->coeffDict());
        kappa_.readIfPresent(this->coeffDict());

        Cb1_.readIfPresent(this->coeffDict());
        Cb2_.readIfPresent(this->coeffDict());
        Cw1_ = Cb1_/sqr(kappa_) + (scalar(1) + Cb2_)/sigmaNut_;
        Cw2_.readIfPresent(this->coeffDict());
        Cw3_.readIfPresent(this->coeffDict());
        Cv1_.readIfPresent(this->coeffDict());
        Cs_.readIfPresent(this->coeffDict());

        return true;
    }

    return false;
}


template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::DnuTildaEff() const
{
    return tmp<volScalarField>::New
    (
        "DnuTildaEff",
        (nuTilda_ + this->nu())/sigmaNut_
    );
}


template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::k() const
{
    // (B:Eq. 4.50)
    const scalar Cmu = 0.09;

    return tmp<volScalarField>::New
    (
        IOobject
        (
            IOobject::groupName("k", this->alphaRhoPhi_.group()),
            this->runTime_.timeName(),
            this->mesh_
        ),
        cbrt(this->fv1(this->chi()))
        *nuTilda_
        *::sqrt(scalar(2)/Cmu)
        *mag(symm(fvc::grad(this->U_))),
        this->nut_.boundaryField().types()
    );
}


template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::epsilon() const
{
    // (B:Eq. 4.50)
    const scalar Cmu = 0.09;
    const dimensionedScalar nutSMALL(sqr(dimLength)/dimTime, SMALL);

    return tmp<volScalarField>::New
    (
        IOobject
        (
            IOobject::groupName("epsilon", this->alphaRhoPhi_.group()),
            this->runTime_.timeName(),
            this->mesh_
        ),
        pow(this->fv1(this->chi()), 0.5)
        *pow(::sqrt(Cmu)*this->k(), 2)
        /(nuTilda_ + this->nut_ + nutSMALL),
        this->nut_.boundaryField().types()
    );
}


template<class BasicTurbulenceModel>
tmp<volScalarField> SpalartAllmarasRC<BasicTurbulenceModel>::omega() const
{
    // (P:p. 384)
    const scalar betaStar = 0.09;
    const dimensionedScalar k0(sqr(dimLength/dimTime), SMALL);

    return tmp<volScalarField>::New
    (
        IOobject
        (
            IOobject::groupName("omega", this->alphaRhoPhi_.group()),
            this->runTime_.timeName(),
            this->mesh_
        ),
        this->epsilon()/(betaStar*(this->k() + k0)),
        this->nut_.boundaryField().types()
    );
}


template<class BasicTurbulenceModel>
void SpalartAllmarasRC<BasicTurbulenceModel>::correct()
{
    if (!this->turbulence_)
    {
        return;
    }

    {
        // Construct local convenience references
        const alphaField& alpha = this->alpha_;
        const rhoField& rho = this->rho_;
        const surfaceScalarField& alphaRhoPhi = this->alphaRhoPhi_;
        fv::options& fvOptions(fv::options::New(this->mesh_));

        eddyViscosity<RASModel<BasicTurbulenceModel>>::correct();

        const volScalarField::Internal Stilda(this->Stilda());
        const volScalarField::Internal fr1(this->fr1());
        const volScalarField fr1Write("fr1",this->fr1());
        
        if (this->mesh_.time().writeTime())
        {
            Info << "write fr1" << endl;
            fr1Write.write();
        }

        tmp<fvScalarMatrix> nuTildaEqn
        (
            fvm::ddt(alpha, rho, nuTilda_)
          + fvm::div(alphaRhoPhi, nuTilda_)
          - fvm::laplacian(alpha*rho*DnuTildaEff(), nuTilda_)
          - Cb2_/sigmaNut_*alpha*rho*magSqr(fvc::grad(nuTilda_))
         ==
            fvm::SuSp(fr1*Cb1_*alpha()*rho()*Stilda, nuTilda_)
          - fvm::Sp(Cw1_*alpha()*rho()*fw(Stilda)*nuTilda_()/sqr(y_), nuTilda_)
          + fvOptions(alpha, rho, nuTilda_)
        );

        nuTildaEqn.ref().relax();
        fvOptions.constrain(nuTildaEqn.ref());
        solve(nuTildaEqn);
        fvOptions.correct(nuTilda_);
        bound(nuTilda_, dimensionedScalar(nuTilda_.dimensions(), Zero));
        nuTilda_.correctBoundaryConditions();
    }

    // Update nut with latest available nuTilda
    correctNut();
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace RASModels
} // End namespace Foam

// ************************************************************************* //
